import React, {Component} from 'react';
import { NavigationContainer, DrawerActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './app/page/Home';
import LoginScreen from './app/page/Login';
import SignUp from './app/page/SignUp';
import { createDrawerNavigator } from '@react-navigation/drawer';
import MyDay from './app/main screen/MyDay';
import TasksAdd from './app/main screen/tasks/TasksAdd';
import Screen1 from './app/page/Work';
import Screen2 from './app/page/Entertaiment';
import {Icon} from 'react-native-elements';
import Profile from './app/main screen/Profile';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

class AppNavigator extends Component {
  render(){
    return(
      <Stack.Navigator initialRouteName='Home'>
        <Stack.Screen 
          name="Home" 
          component={HomeScreen}
          options={{headerShown:false}} /> 
          <Stack.Screen 
          name="Login" 
          component={LoginScreen}
          options={{headerShown:false}} />
          <Stack.Screen 
          name="Signup" 
          component={SignUp}
          options={{headerShown:false}} />
          <Stack.Screen 
          name="MyDay" 
          component={MyDay}
          options={{headerShown:false}} />
          <Stack.Screen 
          name="Add Task" 
          component={TasksAdd}
          options={{headerShown:false}} />
        </Stack.Navigator>

    )
  }
}
export default class App extends Component {
  render(){
  return(
    <NavigationContainer>
      <Drawer.Navigator drawerStyle={{ 
              backgroundColor:'#b0ab66', 
              width: 240,
            }} >
                <Drawer.Screen name="Dashboard" component={AppNavigator}  options={{drawerLabel: 'Task', drawerIcons:({}) => <Icon type='font-awesome' name= 'sun-o' size={40} color="#fff"/> }}/>
                <Drawer.Screen name="Work" options={{drawerIcons:({}) => <Icon type='font-awesome' name= 'building-o' size={40} color="#fff"/> }} component={Screen1} />
                <Drawer.Screen name="Entertaiment" options={{drawerIcons:({}) => <Icon type='font-awesome' name= 'gamepad' size={40} color="#fff"/> }} component={Screen2} />
                <Drawer.Screen name="Your Profile" component={Profile}/>
            </Drawer.Navigator>
  </NavigationContainer>
  )
 }
}