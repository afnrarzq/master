import React, { Component } from 'react'
import { StyleSheet, Text, View, KeyboardAvoidingView } from 'react-native'
import FormRegister from './component/FormRegister';
import LinearGradient from 'react-native-linear-gradient';

export default class SignUp extends Component {
    render() {
        return (
            <LinearGradient colors={gradient} style={styles.gradient}>
                <KeyboardAvoidingView>
                    <View style ={styles.form}>
                        <FormRegister/>
                    </View>
                </KeyboardAvoidingView>
            </LinearGradient>
        )
    }
}
const gradient = ['#001715', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
    gradient:{
        flex:1,
        alignItems: 'center',
    },
    logo: {
        width: 50,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
})
