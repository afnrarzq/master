import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TextInput, } from 'react-native';
import {Icon} from 'react-native-elements';
import axios from 'axios';
import {useNavigation} from '@react-navigation/native';
import Logo from './Logo';

class FormRegister extends Component{
    constructor(props) {
        super(props)
    
    this.state = {
        name: '',
        email: '',
        password:''
    }
}

register = () => {
    const {navigation} = this.props
    axios.post('https://mini-project1.herokuapp.com/api/v1/user/register', {
            name: this.state.name,
            email:this.state.email,
            password: this.state.password
          })
          .then(function(response) {
            console.log(response.data)
            navigation.navigate('MyDay')
            })
          .catch(function (error) {
            console.log(error)
          })
}

    render () {
        return (
            <View style={styles.register}>
                <View style={styles.logo}>
                    <Logo/>
                </View>
                <TextInput style = {styles.input}
                    placeholder = 'Username'
                    onChangeText = {(name)=>{
                        this.setState({name : name})
                    }}
                    value={this.state.name}
                    autoCapitalize='words'
                    >
                </TextInput>
                <TextInput style = {styles.input}
                    placeholder = 'Your Email'
                    onChangeText = {(email)=>{
                        this.setState({email : email})
                    }}
                    value ={this.state.email}
                    autoCapitalize= 'words'
                    >
                </TextInput>
                <TextInput style = {styles.input}
                    placeholder = 'Password'
                    onChangeText = {(password)=>{
                        this.setState({password : password})
                    }}
                    value={this.state.password}
                    secureTextEntry ={true}
                    autoCapitalize= 'none'
                    >
                </TextInput>
           <View style={styles.btnLogin}>
            <TouchableWithoutFeedback onPress={()=> this.register()}>
               <Text style={styles.lgnText}>Sign Up</Text>
            </TouchableWithoutFeedback>
           </View>
           <View style={styles.signIn}>
             <Text style={styles.textOr}> Sign Up with </Text>
             <View style={styles.lgnIcon}>
             <Icon name= 'facebook-square' type='antdesign' size={20}/>
             <Icon name= 'google' type = 'antdesign' size= {20}/>
             <Icon name= 'linkedin-square' type='antdesign' size={20}/>
             </View>
                </View>
            </View>
        )
    }
}

export default function(props){
    const navigation = useNavigation();
    return <FormRegister {...props} navigation={navigation} />
}

const styles = StyleSheet.create({
    register: {
        flex: 1,
        alignItems: 'center',
        height: '45%',
        justifyContent:'center',
    },
    input:{
        width: 290,
        height: 40,
        marginTop: 30,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
    },
    btnLogin : {
        alignItems: 'center',
        justifyContent:'center',
        width: 200,
        height: 60,
        marginTop: 20,
        padding: 20,
        backgroundColor: 'grey',
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        borderBottomLeftRadius:15,
        borderBottomRightRadius: 15,
    },
    loginWith : {
        justifyContent: 'flex-start',
    },
    lgnIcon: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems:'center',
        marginTop: 30,
    },
    textOr: {
        textAlign:'center',
        marginTop: 20,
        letterSpacing: 2,
    },
})