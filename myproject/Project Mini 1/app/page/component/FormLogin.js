import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, TextInput, AsyncStorage,} from 'react-native';
import {Icon} from 'react-native-elements';
import axios from'axios';
import {useNavigation} from '@react-navigation/native';


class FormLogin extends Component{

    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: ''
        }
    }
    login = () => {
        const {navigation} = this.props;
        axios.post('https://mini-project1.herokuapp.com/api/v1/user/login', {
            email:this.state.email,
            password: this.state.password
          })
          .then(function(response) {
            console.log(response.data)
            alert('Logged in');
            AsyncStorage.setItem('userToken', response.data.data.token)
            navigation.navigate('MyDay')
            })
          .catch(function (error) {
            console.log(error.data)
            alert('Email or Password incorect')
          })
    }


    componentDidMount(){
        
           
    }
   
    render(){
    return(
        <View style={styles.action}>
            <TextInput style = {styles.input}
            placeholder = 'Email'
            onChangeText = {(email)=>{
                this.setState({email: email})
            }}
            value={this.state.email}
            autoCapitalize = 'words'
            />
            <TextInput 
            secureTextEntry ={true}
            style = {styles.input}
            placeholder = 'Password'
            onChangeText = {(password)=>{
                this.setState({password: password})
            }}
            value={this.state.password}
            autoCapitalize= 'none'
            />
            <View style={styles.btnLogin}>
            <TouchableWithoutFeedback onPress={()=> this.login()}> 
               <Text style={styles.lgnText}>Sign in</Text>
            </TouchableWithoutFeedback>
         </View>
         <View style={styles.loginWith}>
            <Text style={styles.forgot}> Forgot Password? </Text>
             <Text style={styles.textOr}> Login with </Text>
             <View style={styles.lgnIcon}>
             <Icon name= 'facebook-square' type='antdesign' size={20}/>
             <Icon name= 'google' type = 'antdesign' size= {20}/>
             <Icon name= 'linkedin-square' type='antdesign' size={20}/>
             </View>
         </View>
    </View>
    )
  }
}

export default function(props){
    const navigation = useNavigation();
    return <FormLogin {...props} navigation={navigation} />
}
    
const styles = StyleSheet.create({
    action:{
        flex:2,
        alignItems: 'center',
    },
    input: {
        width: 290,
        height: 50,
            marginTop: 30,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
    },
    lgnText: {
        letterSpacing:2,
        color: 'white',
        fontSize: 16,
    },
    forgot : {
        marginBottom: 30,
        textAlign: 'center',
        fontSize: 13,
        fontWeight:'bold',
        letterSpacing: 1,
        color: 'white',
    },
    btnLogin : {
      width: 200,
      height: 60,
      marginTop: 20,
      marginBottom: 20,
      padding: 20,
      backgroundColor: 'grey',
      alignItems: 'center',
      justifyContent: 'center',
      borderTopLeftRadius:20,
      borderTopRightRadius:20,
      borderBottomLeftRadius:15,
      borderBottomRightRadius: 15,
    },
    loginWith : {
        flex:3,
        justifyContent: 'flex-start'
    },
    lgnIcon: {
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems:'center',
    },
    textOr: {
        textAlign:'center',
        marginBottom: 20,
    },
});