import React, { Component } from 'react'
import {View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { useNavigation } from '@react-navigation/native';

class ButtonHome extends React.Component {
   render() {
      return(
         <View>
         <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate('Login')}>
            <View style={styles.button}>
               <Text style={styles.buttonText}>Login</Text>
            </View>
         </TouchableWithoutFeedback>
         <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate('Signup')}>
            <View style={styles.button}>
               <Text style={styles.buttonText}>Sign up</Text>
            </View>
         </TouchableWithoutFeedback>
         <Text style={styles.direction}> Belum punya account? Silahkan Sign up telebih dahulu  </Text>
         </View>
      )
   }
}

export default function (props){

   const navigation = useNavigation();
      return (
         <ButtonHome {...props} navigation={navigation}/>
      )
   }

const styles = StyleSheet.create({
   button: {
      width: 300,
      height: 60,
      marginBottom: 20,
      padding: 20,
      backgroundColor: 'darkgrey',
      alignItems: 'center',
      justifyContent: 'center',
      borderTopLeftRadius:20,
      borderTopRightRadius:20,
      borderBottomLeftRadius:15,
      borderBottomRightRadius: 15,

         },
   buttonText:{
      color: 'white',
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 16,
   },
   direction: {
      fontSize: 10,
      color: 'white',
      textAlign: 'center',
      marginBottom:10,
   }
});