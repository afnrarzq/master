import React, { Component } from 'react';
import { View, Text, StyleSheet, } from 'react-native';


export default class Body extends Component {
  /*const [title, setTitle] = useState('Welcome to the TODOAPP');
  const [paragraph, setParagraph] = useState('Silahkan daftar atau login');*/
  render(){
        return (
           <View>
             <View>
               <Text style={styles.text}> Welcome to the TODOAPP </Text>
             </View>
             <View>
                 <Text style={styles.textsecond}> Silahkan daftar atau login </Text>
             </View>
           </View>
        );
}
}


const styles = StyleSheet.create({
     text:{
        color: 'white',
        fontSize: 25,
        textAlign: 'center',
        margin: 20,
        fontWeight: 'bold',
    },
    textsecond: {
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
        letterSpacing: 2,
    },
});