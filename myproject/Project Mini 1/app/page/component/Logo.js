import React, {Component} from 'react';
import { View, Text, ImageBackground, StyleSheet, } from 'react-native';
 
export default class Logo extends Component{
      render(){
        return(
          <View>
              <ImageBackground
            source={{
              uri:
                'https://s3-alpha-sig.figma.com/img/0364/a9f2/17293f86040e570ca063aeeb2251da79?Expires=1592179200&Signature=Qa4yjGFYv9dFL4qAr2iPAO6kOTyEB2Wsik1pBmkr1r3LaLW4f5vIY4hFfak8jwhwW~nhhHMUem-6KS6nDr1CiPnb4fhU7kwzy6dPpb9AbtA~vZLz9PYNqvF-bhqeP7ImAvc9G5iyCcm9FeDqPeTYwAfQvrj0~vBm5zwa67k~IeDHzbbJaTtg6s6pqbT0t9CVlaaFfMg6e2m5oFODgaid06vD6CBV51PTSah5WEzjlRmsI9RE-1NOm-v2-qhFQBQy6Y~BWo1IjS8jgjDuAOH6xNaPJWyMs7kqmjzgG7S6G2l~tKKSwnJFvHjBj4iFN2PCldtfmv0mtdTxhDB6ubizFA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
            }}
            style={styles.image}>
            <Text
              style={styles.text}>
              TODO APP
            </Text>
          </ImageBackground>
          </View>
      )
  
    }
}

const styles= StyleSheet.create({
    image: {
        marginTop: 20,
        height: 143,
        width:150,
        position: 'relative',
    },
    text: {
        fontSize: 18,
        fontWeight:'bold',
        color: 'white',
        position: 'absolute',
        bottom:65,
        left:30,
        right:25,
    },
});