import React, {Component} from 'react'
import {View, StyleSheet, StatusBar,} from 'react-native'; 
import Logo from './component/Logo';
import ButtonHome from './component/ButtonHome';
import Body from './component/Body';
import LinearGradient from 'react-native-linear-gradient';

export default class Home extends Component {
  render() {
    return (
      <View style={{
          flex: 1,
        }}>
          <LinearGradient colors={gradient} style={styles.gradient}>
          <StatusBar
          backgroundColor='#1e90ff'
          barStyle='light-content' />
        <View style={styles.headerLogo}>
            <Logo/>
        </View>

        <View style={styles.body}>
            <Body/>
        </View>

        <View style ={styles.button}>
            <ButtonHome />
        </View>
        </LinearGradient>
    </View>
     )
  }
}

const gradient = ['#001715', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
        gradient:{
          flex: 1,
        },
        headerLogo : {
          marginTop: 20,
          alignItems: 'center',
          justifyContent: 'center',
         },
         body: {
           flex:2,
           justifyContent:'center',
           alignItems: 'center',
         },
         button: {
           flex: 3,
           justifyContent: "flex-end",
           alignItems: 'center'
         },
      });