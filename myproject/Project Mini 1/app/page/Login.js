import React, { Component } from 'react'
import { StyleSheet, View, Text, KeyboardAvoidingView, } from 'react-native'
import Logo from './component/Logo';
import FormLogin from './component/FormLogin';
import LinearGradient from 'react-native-linear-gradient';

export default class Login extends Component {
    render(){
    return (
        <KeyboardAvoidingView style={styles.container}>
            <LinearGradient colors={gradient} style={styles.gradient}>
            <View style={styles.headerLogo}>
                <Logo/>
            </View>
            <View style={styles.bodyForm}>
                <FormLogin/>
            </View>
            </LinearGradient>
        </KeyboardAvoidingView>
    )
  }
}

const gradient = ['#001715', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
        },
        gradient: {
            flex: 1,
        },
    headerLogo: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    caption: {
        margin: 20,
        color: 'white',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        letterSpacing: 3,
    },
    bodyForm: {
        flex:2,
        height: 100,
        alignItems: 'center',
        borderRadius: 25,
    },
});
