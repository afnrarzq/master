import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import TodoList from './TodoList';
import moment from 'moment';
import { ScrollView, TouchableWithoutFeedback, FlatList, TouchableHighlight } from 'react-native-gesture-handler';
import { Icon, Header } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import TasksCategory from './tasks/TasksCategory';




class MyDay extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: []
        }
    }
      

    getData = () => {
        axios.get('https://mini-project1.herokuapp.com/api/v1/user/profile')
        .then(res => {
            const data = res.data
            this.setState({ data: data })
        })
    }

    #renderItem = () => (
        <TodoList />
    )

    #keyExtractor = (item, index) => item.id;
    
    render(){

        const date = moment().format('l');
        const { navigation } = this.props;

        return(
            <View style={styles.container}>
            <LinearGradient colors={gradient} style={styles.gradient}>
            <Header 
            containerStyle={{ alignItems: 'center', paddingTop: 0, backgroundColor: '#b0ab66'}}
            leftComponent={<View>
                <TouchableWithoutFeedback style={{ marginHorizontal: 10 }} >
                <Icon name='menu' color='#f2f2f2' size={40} style={styles.toggle} onPress={() => navigation.toggleDrawer()}/>
                </TouchableWithoutFeedback>
                </View>}
            rightComponent={
                <View>
                    <TouchableWithoutFeedback style={{ marginHorizontal: 10 }}>
                        <Icon name='plus' type='font-awesome' color='#f2f2f2' size={40} style={styles.toggle} onPress={() => navigation.navigate('Add Task')}/>
                    </TouchableWithoutFeedback> 
                </View>
            }
            centerComponent={<View style={{
                flexDirection: 'column',
                // margin: 10,
                alignContent: 'center'
            }}>
                <Text style={styles.text}>My Task</Text>
                <Text style={styles.date}>{date}</Text>
            </View>}
            />
                <View style={{
                    flex: 2
                }}>
                <ScrollView>
                    <TodoList />
                </ScrollView> 
                </View>
                <View style={styles.category}>
                        <TouchableHighlight><TasksCategory name='sun-o' >My Day</TasksCategory></TouchableHighlight>
                        <TasksCategory name='building-o' >Work</TasksCategory>
                        <TasksCategory name='plane' >Travel</TasksCategory>
                        <TasksCategory name='gamepad' >Entertaiment</TasksCategory>
                    </View>
            </LinearGradient>
            </View>
        )
    }
}

export default function(props){
    const navigation  = useNavigation();
    return <MyDay {...props} navigation={navigation} />
}

const gradient = ['#001715', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
    gradient: {
        flex: 2,
    },
    toggle: {
        alignSelf: 'flex-start',
    },
    container: {
        flex: 2,
    },
    category: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(242,242,242, 0.5)'
    },
    text: {
        fontFamily: 'Philosopher',
        fontWeight: 'bold',
        fontSize: 30,
        letterSpacing: 3,
        alignItems: 'center', 
        color: '#F2F2F2',
    },
    date: {
        fontFamily: 'Philosopher',
        fontSize: 18,
        display: 'flex',
        alignSelf: 'center',
        textAlign: 'right',
        color: '#F2F2F2',
    }
})