import React, { Component } from 'react';
import axios from 'axios';
import TodoBar from './TodoBar';
import AsyncStorage from '@react-native-community/async-storage'

class TodoList extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: [],
            isComplete: false,
            isImportant: false,
        }
    }

    getData = async () => {
        const AuthToken = await AsyncStorage.getItem('userToken')
        try{
            const res =  await axios.get('https://mini-project1.herokuapp.com/api/v1/tasks?page=1&limit=10', {
                headers: {
                    'Authorization': AuthToken
                }
            })
            if(res !== null){
                const data = res.data.data
                this.setState({ data: data })
                const isImportant = res.data.data.impotance
                const isComplete = res.data.data.completion
                if(isComplete == true){
                    this.setState({ isComplete: true })
                }else if(isImportant == true){
                    this.setState({ isImportant: true })
                }else if(isComplete == false){
                    this.setState({ isComplete: false })
                }else if(isImportant == false){
                    this.setState({ isImportant: false })
                }
            }
        }catch(err){
            console.log(err)
        }
        
        // axios.get(API_TASKS)
        // .then( res => {
        //     const data = res.data
        //     this.setState({
        //         data: data,
        //     })
        // })
    }

    componentDidMount(){
        this.getData()
    }

    componentDidUpdate(){
        this.getData()
    }

    render(){
        return(
            <TodoBar  data={this.state.data} important={this.state.isImportant} complete={this.state.isComplete} markComplete={this.markComplete} delete={this.delete} edit={this.edit} />
        )
    }
}



export default TodoList