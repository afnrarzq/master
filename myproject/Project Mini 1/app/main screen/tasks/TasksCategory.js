import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Icon, Image } from 'react-native-elements'
import { color } from 'react-native-reanimated'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'

const TasksCategory = (props) => {
    return (
        <View style={styles.container}>
            <Icon type='font-awesome' name={props.name} size={25} color='#f2f2f2' />
            <Text style={styles.text}>{props.children}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        alignSelf: 'flex-start',
        margin: 20,
    },
    text: {
        fontFamily: 'Roboto',
        fontSize: 18,
        alignItems: 'center',
        color: '#F2F2F2',
    },
});

export default TasksCategory
