import React, { Component, useState } from 'react'
import { Text, View, StyleSheet, Platform } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { Header, Icon, Button } from 'react-native-elements';
import LeftComponent from './LeftComponent';
import RightComponent from './RightComponent';
import { TextInput, TouchableWithoutFeedback,} from 'react-native-gesture-handler';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import Axios from 'axios';
import moment from 'moment';
import { useNavigation } from '@react-navigation/native';
 


const TasksAdd = () =>{
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [deadline, setDeadline] = useState('');
    const navigation = useNavigation();
    const [name, setName] = useState('');
    const [description, setDescrip] = useState('');
    
    

    // const getData = async() =>{
    //     try{
    //         const AuthToken = await AsyncStorage.getItem('userToken')
    //         if(AuthToken !== null){
    //             setState({ token: AuthToken})
    //         }
    //     }catch(err){
    //         console.log(err);
    //     }

    //     }
    
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'android' ? false : true);
        setDate(currentDate);
        setDeadline(moment(currentDate).format('DD MM YYYY hh:mm'));
    }

    const onChangeText = (val) =>{
        setName(val)
    }

    const onChangeDescrip = (val) => {
        setDescrip(val)
    }

    const addTasks = async () => {
        const AuthToken = await AsyncStorage.getItem('userToken');
        try{
        await Axios.post('https://mini-project1.herokuapp.com/api/v1/tasks', {
            name: name,
            description: description,
            deadline: deadline,
        },
        {
            headers: {
                'Authorization': AuthToken
            }
        }
        )
        .then(res =>{
            if(res.status == 200){
                alert('Task Added Succesfuly!')
                navigation.navigate('MyDay')
            }else{
                alert('Tasks Add Failed')
            }
        })
    }catch(err){
        console.log(err)
    }
        console.log(AuthToken)
        console.log(name)
        console.log(description)
        console.log(deadline)
    }

    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    }

    const showDatePicker = () =>{
        showMode('date')

    }

    const showTimePicker = () => {
        showMode('time')

    }

        return (
            <View style={styles.container}>
                <LinearGradient style={styles.gradient} colors={gradient}>
                    <Header style={{ }}
                    containerStyle={{ alignItems: 'center', paddingTop: 0, backgroundColor:'#b0ab66', }}
                    leftComponent={<LeftComponent cancel={() => navigation.goBack()} />}
                    rightComponent={<RightComponent add={addTasks} />}
                    centerComponent={<Text style={styles.headerText}>Add Task</Text>}
                    />
                    <View>
                        <TextInput style={styles.name}
                        placeholder='Task Name'
                        underlineColorAndroid='#ffffff'
                        placeholderTextColor = "#ffffff"
                        selectionColor="#fff"
                        onChangeText={onChangeText}
                        />
                        <TextInput style={styles.name}
                        placeholder='Description'
                        underlineColorAndroid='#ffffff'
                        placeholderTextColor = "#ffffff"
                        selectionColor="#fff"
                        onChangeText={onChangeDescrip}
                        />
                        <View style={{ 
                            alignSelf: 'center',
                            marginVertical: 20
                         }}>
                            <Text style={styles.headerText} >Deadline</Text>
                        </View>
                        <Button title={date.toDateString()} buttonStyle={styles.date} titleStyle={styles.buttonTitle} type='clear' onPress={showDatePicker} />
                        <Button title={date.toTimeString()} buttonStyle={styles.date} titleStyle={styles.buttonTitle} type='clear' onPress={showTimePicker} />
                        {show && (
                          <RNDateTimePicker
                          value={date}
                          mode={mode}
                          is24Hour={true}
                          display="default"
                          onChange={onChange}
                          neutralButtonLabel="clear"
                        />  
                        )}
                    </View>
                </LinearGradient>
            </View>
        )
    }

export default TasksAdd

const gradient = ['#001715', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    gradient: {
        flex: 1,
    },
    name:{
        color: 'white',
    },
    headerText: {
        fontFamily: 'Philosopher',
        fontWeight: 'bold',
        fontSize: 25,
        alignItems: 'center', 
        color: '#f2f2f2'
    },
    category: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(242,242,242, 0.5)'
    },
    date: {
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingVertical: 10,
        marginHorizontal: 5,
    },
    buttonTitle: { 
        color: '#f2f2f2', 
        fontSize: 18,
        fontFamily: 'Robotica' 
    },

})