import React from 'react'
import { View, Text } from 'react-native'
import { Icon, Button } from 'react-native-elements'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'

const RightComponent = (props) => {
    return (
        // <TouchableWithoutFeedback onPress={props.add} ><Icon type='material-community' name='check' size={35} style={{ marginHorizontal: 10 }} color='#f2f2f2' /></TouchableWithoutFeedback>
        <Button style = {{ backgroundColor: '#b0ab66',}}
         icon={<Icon type='material-community' name='check' size={35} style={{ marginHorizontal: 10,}} color='#f2f2f2' />} onPress={props.add} />
    )
}

export default RightComponent
