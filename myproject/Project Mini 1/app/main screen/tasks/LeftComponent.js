import React from 'react';
import { Icon } from 'react-native-elements';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

const LeftComponent = (props) =>{
    return <TouchableWithoutFeedback onPress={props.cancel} ><Icon type='material-community' name='cancel' color='#f2f2f2' size={35} containerStyle={{ marginHorizontal: 10 }} /></TouchableWithoutFeedback>

}

export default LeftComponent;