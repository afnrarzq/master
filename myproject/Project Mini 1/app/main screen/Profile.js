import React, { Component } from 'react'
import { Text, View, StyleSheet, PermissionsAndroid} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient'
import { Image, Button, Icon, } from 'react-native-elements'
import { ScrollView, TouchableWithoutFeedback, TextInput } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import Axios from 'axios';
import ImagePicker from 'react-native-image-picker';

class Profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            image: '',
            profile: {},
            isEdit: false,
            filePath: {},
            token: '',
            isEditName: false
        }
    }

    componentDidMount = async () => {
        const AuthToken = await AsyncStorage.getItem('https://mini-project1.herokuapp.com/api/v1/user/profile')
        this.setState({ token: AuthToken})
        try{
            await Axios.get('https://mini-project1.herokuapp.com/api/v1/user/profile', {
                headers: {
                    'Authorization': AuthToken
                }
            }).then(res => {
                const data = res.data
                this.setState({ name: data.data.name })
                this.setState({ image: data.data.image_url })
                this.setState({ profile: {uri : `${this.state.image}`} })
            })
        }catch(error){
            console.log('error')
        }
    }

    handleEdit = () => {
        this.setState({ isEdit: true })
    }

    handleEditCancel = () =>{
        this.setState({ isEdit: false })
    }

    handleEditName = () => {
        this.setState({ isEditName: true })
    }


    chooseFile = () => {
        const options = {
          title: 'Select Image',
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else {
            this.setState({
              filePath: response,
              profile: {uri: `${response.uri}`}
            });
          }
        });
      };

      onChangeText = (key, val) =>{
          this.setState({ [key]: val })
      }

    handleSaveImageChange = async () => {
        let dataForm = new FormData();
        dataForm.append('image', {
                name: this.state.filePath.fileName,
                type: this.state.filePath.type,
                uri: this.state.filePath.uri

            })
        try {
            await Axios.post(`https://mini-project1.herokuapp.com/api/v1/user/login` , dataForm,
            {
                headers: {
                    'Authorization': this.state.token,
                    'Accept': 'image/png',
                    'Content-Type': 'multipart/form-data'
                }
            }
            )
            .then(res => {
                const {navigation} = this.props
                const status = res.status
                if(status == 201){
                    alert('Profile Changed')
                    this.setState({ isEdit: false })
                    navigation.navigate('Routes')
                }else{
                    alert('Update Failed')
                }
                console.log(status)
            })
        } catch (error) {
            console.log(error)
        }

        
    }

    handleSaveNameChange = async () =>{
        try {
            await Axios.put('https://mini-project1.herokuapp.com/api/v1/user/update', {
                name: this.state.name
            },
            {
                headers: {
                    'Authorization': this.state.token
                }
            }).then(res => {
                const status = res.status
                if(status == 201){
                    alert('Change Username Succes')
                    this.setState({ isEditName: false })
                }else{
                    alert('Invalid Input Username')
                }
            })
        } catch (error) {
            console.log(error)
        }
    }

    handleLogout = async () =>{
        const { navigation } = this.props;
        try {
            await AsyncStorage.removeItem('userToken')
            .then(navigation.navigate('Routes'))
        } catch (error) {
            alert('error')
        }
        
    }

    render() {
        const { navigation } = this.props;

        let editName;
        if(this.state.isEditName == true){
            editName = (
                <>
                <TextInput style={[styles.text, {backgroundColor: '#fff'}]}
                placeholder={name}
                defaultValue={this.state.name}
                onChangeText={val => this.onChangeText('name', val)}
                placeholderTextColor = "#000"
                selectionColor="#000"
                /> 
                <Button buttonStyle={styles.button} title='Edit Name' onPress={this.handleSaveNameChange} />
                </>
            )
        }else{
            editName = (
                <>
                <Text style= {styles.username}>{this.state.name}</Text>
                <Button buttonStyle={styles.button} title='Edit Name' onPress={this.handleEditName} />
                </>
            )
        }

        let editState;
        if(this.state.isEdit == true){
            editState = (
                <>
                <TouchableWithoutFeedback onPress={() => navigation.toggleDrawer()}>
                <Icon name='menu' color='#f2f2f2' size={40} style={styles.toggle}/>
                </TouchableWithoutFeedback>
                    <View style={styles.profile} >
                        <Image source={this.state.profile} style={styles.profileImage} />
                        {editName}
                    </View>
                    <View style={{
                        margin: 10,
                    }}>
                        <Button buttonStyle={styles.button} title='Change Profile Image' onPress={this.chooseFile.bind(this)} />
                        <Button buttonStyle={styles.button} title='Save Image' onPress={this.handleSaveImageChange} />
                        <Button buttonStyle={styles.button} title='Cancel' onPress={this.handleEditCancel} />
                    </View>
                    </>
            )
        }else{
            editState = (
                <>
                <TouchableWithoutFeedback onPress={() => navigation.toggleDrawer()}>
                <Icon name='menu' color='#f2f2f2' size={40} style={styles.toggle}/>
                </TouchableWithoutFeedback>
                    <View style={styles.profile} >
                        <Image source={this.state.profile} style={styles.profileImage} />
                        <Text style= {styles.username}>{this.state.name}</Text>
                    </View>
                    <View style={{
                        margin: 10,
                    }}>
                        <Button buttonStyle={styles.button} title='Edit Profile' onPress={this.handleEdit} />
                        <Button buttonStyle={styles.button} title='Setting' />
                        <Button buttonStyle={styles.button} title='Sign Out' onPress={this.handleLogout} />
                    </View>
                    </>
            )
        }

        return (
            <View style={styles.container}>
                <LinearGradient colors={gradient} style={styles.gradient} >
                    {editState}
                </LinearGradient>
            </View>
        )
    }
}

export default function(props){
    const navigation  = useNavigation();
    return <Profile {...props} navigation={navigation} />
}

const gradient = ['#001715', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
    container: {
        flex: 2
    },
    gradient: {
        flex: 2
    },
    button: {
        borderRadius: 25,
        marginVertical: 20,
        backgroundColor: 'grey',
    },
    toggle: {
        alignSelf: 'flex-start',
        margin: 10,
    },
    profile: {
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center',
        top: -20
    },

    profileImage: {
        width: 120,
        height: 120,
        maxWidth: 120,
        maxHeight: 120,
        borderRadius: 100,
        margin: 20,
    },

    username: {
        fontSize: 25,
        color: '#f2f2f2',
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        letterSpacing: 2
    },

})