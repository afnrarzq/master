import React, { Component } from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';  
import { CheckBox, Button, Icon, ButtonGroup } from 'react-native-elements';
import moment from 'moment';
import Axios from 'axios';
import { TextInput } from 'react-native-gesture-handler';


class TodoItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            checked: false,
            important: false,
            token: '',
            onEdit: false,
            name: '',
            description: ''
        }
    }

    async componentDidMount(){
        try{
        const AuthToken = await AsyncStorage.getItem('userToken')
        this.setState({ token: AuthToken })
        const res = await Axios.get('https://mini-project1.herokuapp.com/api/v1/tasks/', {
            headers: {
                'Authorization': AuthToken
            }
        })

        if(res !== null){
            const isImpotance = res.data.data.impotance
            const isCompletion = res.data.data.completion
            if(isComplete = true){
                this.setState({ checked: true })
            }else if(isCompletion = false){
                this.setState({ checked: false })
            }else if(isImpotance = true){
                this.setState({ important: true })
            }else if(isImpotance = false){
                this.setState({ important: false })
            }
        }

    }catch(err){
        console.log('error')
    }
    }

    onChangeText = (key, val) => {
        this.setState({ [key]: val })
    }

    // onCompleteChange = async (id) => {
    //     try{
    //         await Axios.put(`https://mini-project1.herokuapp.com/api/v1/tasks/${id}`, {completion : true}, {headers: { 'Authorization': this.state.token }})
    //     }catch(err){
    //         console.log('error')
    //     }
    // }

    // onImportantChange = async (id) => {
    //     try{
    //         await Axios.put(`https://mini-project1.herokuapp.com/api/v1/tasks/${id}`, {importance: true}, {headers: { 'Authorization': this.state.token }})
    //     }catch(err){
    //         console.log(error)
    //         console.log(this.state.token)
    //         console.log('error')
    //     }
    // } 

    onDeleteChange = async (id) => {
        try{
             await Axios.delete(`https://mini-project1.herokuapp.com/api/v1/tasks/${id}`, {headers: { 'Authorization': this.state.token }}).then(res => {
                if(res.status == 201){
                    alert('tasks deleted')
                } else{
                    alert('error')
                }
            })
        }catch(error){
            console.log(error)
            console.log(this.state.token)
            console.log('error')
        }
    }

    handleEdit = () => {
        this.setState({ onEdit: true })
    }

    handleEditCancel = () => {
        this.setState({ onEdit: false })
    }

    onEditChange = async (id, name, description) => {
        try{
            await Axios.put(`https://mini-project1.herokuapp.com/api/v1/tasks/${id}`, {
                name: this.state.name,
                description: this.state.description,
                deadline: deadline
            },
            {
                headers: {
                    'Authorization': this.state.token
                }
            }).then(res => {
                if(res.status == 201){
                    alert('Change Task Succes')
                    this.setState({ onEdit: false })
                }else{
                    alert('Invalid')
                }
            })
        }catch(error){
            console.log('error')
        }
        console.log(this.state.token)
        console.log(this.state.name)
        console.log(this.state.description)

//     markImportant = (id) =>{
//         if(!this.state.important){
//             this.setState({
//                 important: true
//             })
//             Axios.put(
//               `https://mini-project1.herokuapp.com/api/v1/tasks/19${id}`,
//               {importance: true},
//               {headers: {Authorization: this.state.token}}
//             );
//         }else{
//             this.setState({
//                 important: false
//             })
//             Axios.put(
//               `https://mini-project1.herokuapp.com/api/v1/tasks/19${id}`,
//               {importance: false},
//               {headers: {Authorization: this.state.token}}
//             );
//         }
//         // console.log(this.state.token)
//     }
// }

//     markComplete(id){
//         if(!this.state.checked){
//             this.setState({
//                 checked: true,
//             })
//             Axios.put(`https://mini-project1.herokuapp.com/api/v1/tasks/19${id}`,{completion: true },{ headers: {'Authorization' : this.state.token} })
//         } else {
//             this.setState({
//                 checked: false,
//             })
//             Axios.put(`https://mini-project1.herokuapp.com/api/v1/tasks/19${id}`, {completion: false},{ headers: {'Authorization' : this.state.token} })
//         }

}


    render(){
        const {name, description, deadline, id, important, completed} = this.props.data;

        const createTime = moment(deadline).endOf('day').fromNow(); 

        let edit;
        if(this.state.onEdit == true){
            edit = (
                <View style={styles.rowText}>
                <TextInput style={[styles.text, {backgroundColor: '#fff'}]}
                // placeholder={name}
                defaultValue={name}
                onChangeText={val => this.onChangeText('name', val)}
                placeholderTextColor = 'black'
                selectionColor="#000"
                />
                <TextInput style={[styles.textDes], {backgroundColor: '#fff'}}
                // placeholder={description}
                defaultValue={description}
                onChangeText={val => this.onChangeText('description', val)}
                placeholderTextColor = "#000"
                selectionColor="#000"
                />
                {/* <Button
                buttonStyle={{ backgroundColor: '#ec823a', }}
                icon={<Icon type='material-community' name='content-save' size={25} color='#162447'  />}
                />
                {/* <Text style={styles.text}>{name}</Text>
                <Text style={styles.textDes}>{description}</Text> */} 
                </View>
            )
        }else{
            edit = (
                <View style={styles.rowText}>
                <Text style={styles.text}>{name}</Text>
                <Text style={styles.textDes}>{description}</Text>
                </View>
            )
        }

        let editButtonChange;
        if(this.state.onEdit == true){
            editButtonChange = (
            <View style={{ flexDirection: 'row', justifyContent: 'center' }} >
                <Button 
                icon={<Icon type='font-awesome' name='hdd-o' size={25} color='#162447'  />}
                // type='clear'
                // buttonStyle={{
                //     alignSelf: 'center',
                //     marginVertical: 2
                // }}
                buttonStyle={{ backgroundColor: '#ec823a', }}
                containerStyle={{
                    alignSelf: 'center',
                    marginHorizontal: 5
                }}
                // onPress={this.onEditChange}
                onPress={() => this.onEditChange(id)}
                />
                <Button 
                // type= 'clear'
                icon={<Icon name='cancel' size={25} color='#f2f2f2' />}
                buttonStyle={{ backgroundColor: 'grey', }}
                // buttonStyle={{
                //     alignItems: 'center',
                //     marginVertical: 2,
                // }}
                containerStyle={{
                    alignSelf: 'center',
                    marginHorizontal: 5
                }}
                // onPress={() => this.onDeleteChange(id)}
                onPress={this.handleEditCancel}
                />
                </View>
                )
        }else{
            editButtonChange = (
            <View style={{ flexDirection: 'row', justifyContent: 'center' }} >
                <Button 
                icon={<Icon type='font-awesome' name='pencil' size={25} color='#f2f2f2'  />}
                // type='clear'
                // buttonStyle={{
                //     alignSelf: 'center',
                //     marginVertical: 2
                // }}
                buttonStyle={{ backgroundColor: 'grey', }}
                containerStyle={{
                    alignSelf: 'center',
                    marginHorizontal: 5
                }}
                onPress={this.handleEdit}
                />
                <Button 
                // type= 'clear'
                icon={<Icon name='delete' size={25} color='#f2f2f2' />}
                buttonStyle={{ backgroundColor: 'grey', }}
                // buttonStyle={{
                //     alignItems: 'center',
                //     marginVertical: 2,
                // }}
                containerStyle={{
                    alignSelf: 'center',
                    marginHorizontal: 5
                }}
                onPress={() => this.onDeleteChange(id)}
                />
                </View>
                )
        }
    
        return (
          <View style={styles.rowContainer}>
            {/* <View style={styles.rowText}>
                <Text style={styles.text}>{name}</Text>
                <Text style={styles.textDes}>{description}</Text>
                </View> */}
            {edit}
            <View style={styles.editButton}>
              <View style={{flexDirection: 'row'}}>
                <CheckBox
                  checked={completed}
                  // onPress={() => this.markComplete(id)}
                  onPress={() => this.onCompleteChange(id)}
                  iconType="material-community"
                  checkedIcon="check-circle-outline"
                  uncheckedIcon="checkbox-blank-circle-outline"
                  checkedColor="#f2f2f2"
                  uncheckedColor="#f2f2f2"
                  size={40}
                  containerStyle={styles.checkbox}
                />
                <CheckBox
                  iconType="material-community"
                  checked={important}
                  // onPress={() => this.markImportant(id)}
                  onPress={() => this.onImportantChange(id)}
                  checkedIcon="star"
                  uncheckedIcon="star-outline"
                  checkedColor="yellow"
                  size={40}
                  containerStyle={styles.importantCheckbox}
                  uncheckedColor="#f2f2f2"
                />
              </View>
              <View style={{alignSelf: 'center'}}>
                {/* <View> */}
                <Text style={styles.subText}>Deadline: {createTime}</Text>
              </View>
            </View>
            {editButtonChange}
          </View>
        );
    }
}

// Axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

export default TodoItem;

const styles = StyleSheet.create({
    rowContainer: {
      flexDirection: 'column',
      backgroundColor: '#b0ab66',
    //   height: 100,
      padding: 10,
      marginRight: 10,
      marginLeft: 10,
      marginTop: 10,
      borderRadius: 4,
      shadowOffset:{  width: 1,  height: 1,  },
      shadowColor: '#CCC',
      shadowOpacity: 1.0,
      shadowRadius: 1
    },

    textBar: {
        alignContent: 'center',
        justifyContent: 'center',
        margin: 10
    },

    editButton: {
        flexDirection: 'row',
        alignContent: 'flex-end',
        // alignContent: 'center',
        justifyContent: 'space-between',
        marginVertical: 10
        // marginTop: 10
    },

    checkbox: {
        alignSelf: 'center',
        margin: 0,
        marginLeft: 0,
        marginRight: 0,
        padding: 0,
        // width: 50,
        // height: 50,
    },

    importantCheckbox: {
        alignSelf: 'center',
        margin: 0,
        padding: 0,
        marginLeft: 0,
        marginRight: 0,
        // width: 40,
        // height: 40,
    },

    text: {
        fontFamily: 'Roboto',
        fontSize: 21,
        fontWeight: 'bold',
        display: 'flex',
        alignItems: 'center',
        color: '#777',
        textAlign: 'center',
        margin: 2
    },
    textDes: {
        fontFamily: 'Roboto',
        fontSize: 15,
        display: 'flex',
        alignItems: 'center',
        color: '#000000',
        textAlign: 'left',
        margin: 2
    },
    subText: {
        // fontFamily: 'Philosopy',
        fontSize: 16,
        fontWeight: 'bold',
        // alignItems: 'flex-end',
        color: '#c70039',
        textAlign: 'right',
    },
rowText: {
    flex: 4,
    flexDirection: 'column'
  }
})