import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { HeaderBackButton } from '@react-navigation/stack';

import Icon from 'react-native-vector-icons/Ionicons';

import HomeScreen from './src/screen/HomeScreen';
import LoginScreen from './src/screen/LoginScreen';
import RegisterScreen from './src/screen/RegisterScreen';
import ProfileScreen from './src/screen/ProfileScreen';
import MainScreen from './src/screen/MainScreen';
import MyTab from './src/screen/MyTab';

// import {
// //   SafeAreaView,
// //   StyleSheet,
// //   ScrollView,
// //   View,
// //   Text,
//   Button, 
// //   StatusBar,
// } from 'react-native';

const Stack = createStackNavigator();

const App = ({navigation}) => {
  return(
  <NavigationContainer>
  <Stack.Navigator initialRouteName='Home'>
    <Stack.Screen name= "Home" component={HomeScreen} options={{headerShown:false}}/>
    <Stack.Screen name= "Login" component={LoginScreen} options={{headerShown:false}}/>
    <Stack.Screen name= "Register" component={RegisterScreen} options={{headerShown:false}}/>
    <Stack.Screen name= "Tab" component={MyTab} options = {{ headerShown: false }}/>
  </Stack.Navigator>
  </NavigationContainer>
  );
}
export default App;

