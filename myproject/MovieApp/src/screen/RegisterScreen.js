import React, {useEffect} from 'react'
import { StyleSheet, Text, View, KeyboardAvoidingView } from 'react-native'
import Logo from './Component/Logo';
import FormRegister from './Component/FormRegister';
import { ActivityIndicator } from 'react-native-paper';

const RegisterScreen = ({navigation}) => {
    const [isLoading, SetIsLoading] = React.useState(true);
    const [userToken, setUserToken] = React.useState(null);
    
        useEffect(() => {
            setTimeout(() => {
                SetIsLoading(false);
            }, 1000) ;
        }, []);
    
        if( isLoading ) {
            return(
                <View style={{ flex: 1, justifyContent:'center', alignItems: 'center',}}>
                    <ActivityIndicator size="large"/>
                </View>
            );
        }

    return (
        <KeyboardAvoidingView style={styles.container}>
        <View style={styles.headerLogo}>
            <Logo/>
        </View>
         <View style={styles.bodyForm}>
            <FormRegister navigation={navigation}/>
        </View>
    </KeyboardAvoidingView>
    )
}

export default RegisterScreen;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#c38e4a',
    },
    headerLogo: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    bodyForm: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },

})
