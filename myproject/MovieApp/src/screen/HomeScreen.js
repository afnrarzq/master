import React, { useEffect } from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'
import Logo from './Component/Logo';
import ButtonHome from './Component/ButtonHome';
import { ActivityIndicator } from 'react-native-paper';


const HomeScreen = ({navigation}) => {
    const [isLoading, SetIsLoading] = React.useState(true);
    
        useEffect(() => {
            setTimeout(() => {
                SetIsLoading(false);
            }, 1000) ;
        }, []);
    
        if( isLoading ) {
            return(
                <View style={{ flex: 1, justifyContent:'center', alignItems: 'center',}}>
                    <ActivityIndicator size="large"/>
                </View>
            );
        }
    return (
      <View style={styles.container}>
           <View style={styles.headerLogo}>
                <Logo/>
            </View>
            <View style={styles.caption}>
                <Text style={styles.textCaption}> Get the best Movie, from Now ! </Text>
                </View>
            <View style={styles.button}>
                <ButtonHome navigation={navigation}/>
            </View>
     </View>
    );
  };

export default HomeScreen;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#c38e4a',
    },
    headerLogo: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    caption: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textCaption:{
        fontSize: 20,
        color: '#654317',
        fontStyle: 'italic',
        fontFamily: 'FontAwesome5_Regular',
        fontWeight: 'bold',
        letterSpacing: 1,
    },
    button: {
        flex: 3,
        justifyContent: "center",
           alignItems: 'center'
    }
})
