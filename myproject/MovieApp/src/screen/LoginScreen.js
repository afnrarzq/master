import React, {useEffect} from 'react';
import { StyleSheet, Text, View, Button, KeyboardAvoidingView } from 'react-native';
import Logo from './Component/Logo';
import FormLogin from './Component/FormLogin';
import { ActivityIndicator } from 'react-native-paper';


const LoginScreen = ({navigation}) => {
        const [isLoading, SetIsLoading] = React.useState(true);
    
        useEffect(() => {
            setTimeout(() => {
                SetIsLoading(false);
            }, 1000) ;
        }, []);
    
        if( isLoading ) {
            return(
                <View style={{ flex: 1, justifyContent:'center', alignItems: 'center',}}>
                    <ActivityIndicator size="large"/>
                </View>
            );
        }
    return (
        <KeyboardAvoidingView style={styles.container}>
            <View style={styles.headerLogo}>
                <Logo/>
            </View>
             <View style={styles.bodyForm}>
                <FormLogin navigation={navigation}
                />
            </View>
        </KeyboardAvoidingView>
    );
  };

export default LoginScreen;

const styles = StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            backgroundColor:'#c38e4a',
        },
    headerLogo: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    bodyForm: {
        flex:2,
        height: 100,
        alignItems: 'center',
        borderRadius: 25,
    },
});
