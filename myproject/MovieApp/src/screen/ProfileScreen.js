import React from 'react'
import { StyleSheet, Text, View, KeyboardAvoidingView } from 'react-native'
import FormProfile from './Component/FormProfile';

const ProfileScreen = ({navigation}) => {
    return (
        <KeyboardAvoidingView style={styles.container}>
        <View style={styles.Profile}>
            <FormProfile navigation ={navigation} />
        </View>
        </KeyboardAvoidingView>
    )
}

export default ProfileScreen;

const styles = StyleSheet.create({
    container :{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#c38e4a',
    },
    
})
