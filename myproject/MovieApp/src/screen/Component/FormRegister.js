import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableWithoutFeedback, } from 'react-native'
import {Icon} from 'react-native-elements';
import axios from 'axios';
import devicesStorage from '../../devicestorage';

const FormRegister = ({navigation}) => {
    const [name, setName] = useState ('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleName = (val) => {
        setName(val)
    }
    const handleEmail = (val) => {
        setEmail(val)
    }
    const handlePassword = (val) => {
        setPassword(val)
    }

    const handleRegister = async () => {
        try {
            const register = await axios({
                method:'POST',
                headers:{
                    'Content-Type': 'application/json',
                },
               url: 'https://mini-project-02.herokuapp.com/api/v1/user/register',
                data:JSON.stringify({
                    name,
                    email,
                    password,
            })
        })
        devicesStorage.savetoken('token',register.data.data.token)
        console.log(name,email,password)
        if(register.data){
            const token = await devicesStorage.gettoken('token')
            console.log('token',token)
            navigation.replace('Tab')
        }
    }
    catch(error){
        alert('Register Gagal')
    }
}
    return (
        <View style={styles.register}>
        <TextInput style = {styles.input}
            placeholder = 'Username'
            onChangeText = {(val)=> handleName(val)}
            autoCapitalize='words'
            >
        </TextInput>
        <TextInput style = {styles.input}
            placeholder = 'Your Email'
            onChangeText = {(val)=> handleEmail(val)}
            autoCapitalize= 'none'
            >
        </TextInput>
        <TextInput style = {styles.input}
            placeholder = 'Password'
            onChangeText = {(val)=> handlePassword(val)}
            secureTextEntry ={true}
            autoCapitalize= 'none'
            >
        </TextInput>
   <View style={styles.btnLogin}>
    <TouchableWithoutFeedback 
    onPress={()=> handleRegister()}
    >
       <Text style={styles.lgnText}>Sign Up</Text>
    </TouchableWithoutFeedback>
   </View>
   <View style={styles.signIn}>
     <Text style={styles.textOr}> Sign Up with </Text>
     <View style={styles.lgnIcon}>
     <Icon name= 'facebook-square' type='antdesign' size={20}/>
     <Icon name= 'google' type = 'antdesign' size= {20}/>
     <Icon name= 'linkedin-square' type='antdesign' size={20}/>
     </View>
        </View>
    </View>
)
}

export default FormRegister;

const styles = StyleSheet.create({
    register: {
        alignItems: 'center',
        height: '45%',
        justifyContent:'center',
    },
    input:{
        width: 290,
        height: 40,
        marginTop: 30,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
    },
    btnLogin : {
        alignItems: 'center',
        justifyContent:'center',
        width: 200,
        height: 60,
        marginTop: 20,
        padding: 20,
        backgroundColor: '#654317',
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        borderBottomLeftRadius:15,
        borderBottomRightRadius: 15,
    },
    lgnText: {
        color: 'white',
    },
    loginWith : {
        justifyContent: 'flex-start',
    },
    lgnIcon: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems:'center',
        marginTop: 30,
    },
    textOr: {
        textAlign:'center',
        marginTop: 20,
        letterSpacing: 2,
    },
})
