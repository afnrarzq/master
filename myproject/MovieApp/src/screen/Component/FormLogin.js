import React, {useState} from 'react'
import { StyleSheet, Text, View, TextInput,TouchableWithoutFeedback, } from 'react-native'

import {Icon} from 'react-native-elements';
import axios from 'axios';
import devicesStorage from '../../devicestorage';

const FormLogin = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const handleEmail = (val)=>{
        setEmail(val)
    }
    const handlePassword = (val)=> {
        setPassword(val)
    }
    const handleLogin = async () => {
        try {
            const login = await axios({
                method:'POST',
                headers:{
                    'Content-Type': 'application/json',
                },
                url:'https://mini-project-02.herokuapp.com/api/v1/user/login',
                data:JSON.stringify({
                    email,
                    password,
                })
            })
            devicesStorage.savetoken('token',login.data.data.token)
            console.log(email,password)
            if(login.data){
                const token = await devicesStorage.gettoken('token')
                console.log('token',token)
                navigation.replace('Tab')
            }
        }
        catch(error){
            alert('Login Gagal')
        }
    }
    return (
        <View style={styles.action}>
        <TextInput style = {styles.input}
        placeholder = 'Email'
        onChangeText = {(val)=> handleEmail(val)}
        autoCapitalize = 'words'
        />
        <TextInput 
        secureTextEntry ={true}
        style = {styles.input}
        placeholder = 'Password'
        onChangeText = {(val) => handlePassword(val)}
        autoCapitalize= 'none'
        />
        <View style={styles.btnLogin}>
        <TouchableWithoutFeedback 
        onPress={()=> handleLogin()}>
           <Text style={styles.lgnText}>Sign in</Text>
        </TouchableWithoutFeedback>
     </View>
     <View style={styles.loginWith}>
        <Text style={styles.forgot}> Forgot Password? </Text>
         <Text style={styles.textOr}> Login with </Text>
         <View style={styles.lgnIcon}>
         <Icon name= 'facebook-square' type='antdesign' size={20}/>
         <Icon name= 'google' type = 'antdesign' size= {20}/>
         <Icon name= 'linkedin-square' type='antdesign' size={20}/>
         </View>
     </View>
</View>
)
}

export default FormLogin;

const styles = StyleSheet.create({
    action:{
        flex:2,
        alignItems: 'center',
    },
    input: {
        width: 290,
        height: 50,
            marginTop: 30,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
    },
    lgnText: {
        letterSpacing:2,
        color: 'white',
        fontSize: 16,
    },
    forgot : {
        marginBottom: 30,
        textAlign: 'center',
        fontSize: 13,
        fontWeight:'bold',
        letterSpacing: 1,
        color: '#654317',
    },
    btnLogin : {
      width: 200,
      height: 60,
      marginTop: 20,
      marginBottom: 20,
      padding: 20,
      backgroundColor: '#654317',
      alignItems: 'center',
      justifyContent: 'center',
      borderTopLeftRadius:20,
      borderTopRightRadius:20,
      borderBottomLeftRadius:15,
      borderBottomRightRadius: 15,
    },
    loginWith : {
        flex:3,
        justifyContent: 'flex-start'
    },
    lgnIcon: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems:'center',
    },
    textOr: {
        textAlign:'center',
        marginBottom: 20,
        color: '#654317',
    },
})
