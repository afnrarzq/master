import React from 'react'
import { StyleSheet, Text, View, Image, } from 'react-native'

export default function Logo() {
    return (
        <View>
         <Image
            source={require('./asset/image/movie-icon.png')}
                style={styles.image}/>
                <Text style={styles.textHeader}>
                    Movie Review
                </Text>
    </View>
    )
}


const styles = StyleSheet.create({
    image: {
        marginTop: 20,
        height: 143,
        width:150,
        position: 'relative',
    },
    textHeader: {
        fontFamily: 'FontAwesome5_Regular',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 20,
        lineHeight: 28,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color: '#F2F2F2',
    },
})
