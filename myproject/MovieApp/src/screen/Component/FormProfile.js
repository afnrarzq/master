import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableWithoutFeedback, } from 'react-native';
import axios from 'axios';
import { set } from 'react-native-reanimated';

const FormProfile = () => {
    const [id, setId] = useState ('');
    const [name, setName] = useState ('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [edit, setEdit] = useState('');

    useEffect(() => {
        handleProfile()
    }, []);
    const handleProfile = async () => {
        try {
            const profile = await axios({
                method:'GET',
                headers:{
                    authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJkdW1teUBtYWlsLmNvbSIsImlhdCI6MTU5MDg0Mjg4Nn0.YgJllwYEq6mT3UuLShjvbkP9j0URgM0SewruDt9NLLw'
                },
                url: 'https://mini-project1.herokuapp.com/api/v1/user/profile'
        })
        console.log('profile',profile.data)
        setId(profile.data.data.id)
        setPict(profile.data.data.owner.image)
        setName(profile.data.data.owner.name)
        setEmail(profile.data.data.email)
        setPassword(profile.data.data.password)
        console.log('name', profile.data.data.owner.name)
    }
    catch(error){
        alert('Failed')
    }
}
console.log('Id',id)

    const handleEdit = async () => {
        try {
            const edit = await axios ({
                method: 'PUT',
                headers:{
                    authorization:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTQsImVtYWlsIjoidGVzdDEyQHRlc3QuY29tIiwiaWF0IjoxNTkyMjAzMTM0fQ.5xqgjMlqZOvuf5e7_IB_JZ8cRdMYrEjascohSLK0HUM'
                },
                url: `https://mini-project1.herokuapp.com/api/v1/user/update/${id}`,
                data:{
                    name:name,
                }
    })
    console.log(edit)
       }
catch(error){
    alert('Edit Gagal')
    console.log(error)
}
}
    return (
        <View style={styles.profile}>
            <View style={styles.profileImage}>
                {/* <Image style={styles.image} 
                {pict} /> */}
            </View>
        <TextInput style = {styles.input}
            onChangeText={(name) => (setName(name))}>
          {name}  
        </TextInput>
        <TextInput style = {styles.input}
            // onChangeText={(email) => (setEmail(email))}
            >
        </TextInput>
        <TextInput style = {styles.input}
        // onChangeText={(password) => (setPassword(password))}
        // secureTextEntry={true}
        >
        </TextInput>
   <View style={styles.btnEdit}>
    <TouchableWithoutFeedback 
   onPress={()=> handleEdit()}
    >
       <Text style={styles.editText}>Edit Profile</Text>
    </TouchableWithoutFeedback>
   </View>
</View>
)
}

export default FormProfile;

const styles = StyleSheet.create({
    profile: {
        flex: 1,
        alignItems: 'center',
        height: '45%',
        justifyContent:'center',
    },
    profileImage:{
        width: 150,
        height: 150,
        borderRadius: 80,
        overflow: 'hidden',
    },
    image:{
        flex: 1,
        width: undefined,
        height: undefined,
    },
    input:{
        width: 290,
        height: 40,
        marginTop: 30,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
    },
    // password: {
        
    // }
    btnEdit : {
        alignItems: 'center',
        justifyContent:'center',
        width: 200,
        height: 60,
        marginTop: 20,
        padding: 20,
        backgroundColor: '#654317',
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        borderBottomLeftRadius:15,
        borderBottomRightRadius: 15,
    },
    editText: {
        color: 'white',
    },
})
