import React from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback, } from 'react-native'
import {Icon} from 'react-native-elements';

  const ButtonHome = ({navigation}) => {
    return (
        <View>
        <TouchableWithoutFeedback  onPress= {() => navigation.navigate("Login")}>
           <View style={styles.button}>
              <Text style={styles.buttonText}>Login</Text>
           </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={()=> navigation.navigate('Register')}>
           <View style={styles.button}>
              <Text style={styles.buttonText}>Sign up</Text>
           </View>
        </TouchableWithoutFeedback>
        <Text style={styles.direction}> Do you need Account? Sign up Now !  </Text>
        <View style={styles.signIn}>
             <Text style={styles.textOr}> Signup or Login with </Text>
             <View style={styles.lgnIcon}>
             <Icon name= 'facebook-square' type='antdesign' size={20}/>
             <Icon name= 'google' type = 'antdesign' size= {20}/>
             <Icon name= 'linkedin-square' type='antdesign' size={20}/>
             </View>
                </View>
        </View>
     )
  }
  export default ButtonHome;

const styles = StyleSheet.create({
        button: {
           width: 300,
           height: 60,
           marginBottom: 20,
           padding: 20,
           backgroundColor: '#654317',
           alignItems: 'center',
           justifyContent: 'center',
           borderTopLeftRadius:20,
           borderTopRightRadius:20,
           borderBottomLeftRadius:15,
           borderBottomRightRadius: 15,
     
              },
        buttonText:{
           color: 'white',
           textAlign: 'center',
           fontWeight: 'bold',
           fontSize: 16,
        },
        direction: {
           fontSize: 10,
           color: '#654317',
           textAlign: 'center',
           fontWeight: 'bold',
           marginBottom:10,
        },
        lgnIcon: {
            justifyContent: 'space-around',
            flexDirection: 'row',
            marginTop: 20,
        },
        textOr: {
            textAlign:'center',
            color: '#654317',
            marginTop: 20,
            fontWeight: 'bold',
            letterSpacing: 1,
        },
     });
