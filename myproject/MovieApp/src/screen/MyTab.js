import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import ProfileScreen from './ProfileScreen';
import MainScreen from './MainScreen';
import ReviewScreen from './ReviewScreen';

import Icon from 'react-native-vector-icons/Entypo'

const Tab = createBottomTabNavigator();

 const Tabscreen = () => {
    return(
    // <NavigationContainer>
    <Tab.Navigator initialRouteName='Main' style={{
        color: 'blue',
    }} >
        <Tab.Screen 
        name= "Main" 
        component={MainScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: () => (
            <Icon name="home" color={'blue'} size={22}/>
          ),
        }}
        />
        <Tab.Screen 
         name= "Review" 
         component={ReviewScreen}
         options={{
           tabBarLabel: "view",
           tabBarIcon: ()=> {
             <Icon name="wechat" color={'blue'} size={22}/>
           }
         }}
         />
        <Tab.Screen 
        name= "Profile" 
        component={ProfileScreen}
        options={{
          tabBarLabel: 'User',
          tabBarIcon: () => (
            <Icon name="user" color={'blue'} size={22}/>
          ),
        }}/>
    </Tab.Navigator>
    // </NavigationContainer>
    );
  }
  
  export default Tabscreen;

