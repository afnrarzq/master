import React, { useEffect, useState } from 'react'
import { StyleSheet, View, FlatList, } from 'react-native'
import { ActivityIndicator } from 'react-native-paper';
import axios from 'axios';


const  ReviewScreen = () => {
    const [isLoading, SetIsLoading] = React.useState(true);
    const [coment, setComent] =  useState ('');

    const getReview = async ()=> {
        try {
            const review = await axios({
                method:'GET',
                headers:{
                    authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjAsImVtYWlsIjoidGVzdDEyNUB0ZXN0LmNvbSIsImlhdCI6MTU5MjIwMzcyNH0.SyHNp0G2LTFLaZy5ewAkoJP1aMU1pB7m73ONXEqGfL8'
                },
                url: 'https://mini-project-02.herokuapp.com/api/v1/movie/review/show/1'
        })
        console.log('review',review.data.rows)
    }
        catch(e){
            console.log(e)
        }
    }

    const handleReview = async () => {
        try{
            const coment = await axios ({
                method:'GET',
                headers: {
                "Content-Type": "application/json",
                },
                url: `https://mini-project-02.herokuapp.com/api/v1/movie/review/show/1`,
            })
            console.log(coment)
            setComent(coment.data.data.rows)
            console.log('data', coment.data.data.rows)
        }

        catch(e){
            console.log(e)
        }
    }
    console.log(coment);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false);
        }, 1000) ;
    }, []);

    if( isLoading ) {
        return(
            <View style={{ flex: 1, justifyContent:'center', alignItems: 'center',}}>
                <ActivityIndicator size="large"/>
            </View>
        );
    }
    return (
        <View style={styles.container}>
         <FlatList data={coment} 
                renderItem = {({item}) => {
                    return(
                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            margin: 20,

                        }}>
                            <Text style={styles.title}> {item.coment} </Text>
                        </View>
                    )
                }}
            />
        </View>
    )
}

export default ReviewScreen;

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#c38e4a',
    }
})
