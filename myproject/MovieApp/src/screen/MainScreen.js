import React, { useEffect, useState, } from 'react'
import { StyleSheet, View,  Text, TextInput, FlatList, Image} from 'react-native'
import { ActivityIndicator } from 'react-native-paper';
import axios from 'axios';
import { set } from 'react-native-reanimated';
import { WebView } from 'react-native-webview'; 

 const MainScreen = ({navigation}) => {
    const [isLoading, SetIsLoading] = React.useState(true);
    const [title, setTitle] = useState('');
    const [page, setPage] = useState ('');
    const [find, setFind] = useState ('');

    const search = async ()=> {
        try{
            const search = await axios ({
                method:'GET',
                headers: {
                "Content-Type": "application/json",
                },
                url: `https://mini-project-02.herokuapp.com/api/v1/movie/show?movie=${title},&page=1&limit=10`,
            })
            console.log(search)
            setFind(search.data.data.rows)
            console.log('data', search.data.data.rows)
        }
        catch(e){
            console.log(e)
        }
    }
    console.log(title);
    
    
    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false);
        }, 1000) ;
    }, []);

    if( isLoading ) {
        return(
            <View style={{ flex: 1, justifyContent:'center', alignItems: 'center',}}>
                <ActivityIndicator size="large"/>
            </View>
        );
    }
    return (
        <View style={styles.container}>
               <TextInput
                style={styles.searchbox}
                placeholder='Enter the movie'
                onChangeText={(title) => (setTitle(title))}
                onSubmitEditing={search}
                value= {title}

            />
            <Text style={styles.titleHeader}> Movie Review </Text>
            <FlatList data={find} 
                renderItem = {({item}) => {
                    console.log(item.poster)
                    return(
                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            margin: 20,

                        }}>
                            <Text style={styles.title}> {item.title} </Text>
                            <Image
                            style={{
                                width: 300,
                                height: 400,
                            }}
                               source={{
                                uri: `${item.poster}`,
                              }}
                            />
                            <Text style={styles.genre}> {item.genre} </Text>
                            <WebView style={{
                                width: 200,
                                height: 300,
                            }} 
                                source={{
                                uri: `${item.trailer}`,
                            }}
                            />
                        </View>
                    )
                }}
            />
        </View>
    )
}

export default MainScreen;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#c38e4a',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 70,
        paddingHorizontal: 20,
    },
    titleHeader:{ 
        color: '#654317',
        fontSize: 32,
        fontWeight: '700',
        textAlign: 'center',
        marginBottom: 20,
    },
    title: {
        backgroundColor: '#654317',
        color: '#fff',
        fontSize: 25,
        fontWeight: '700',
        textAlign: 'center',
        marginBottom: 20,
        borderRadius: 10,
    },
    genre: {
        backgroundColor: '#654317',
        color: '#fff',
        fontSize: 25,
        fontWeight: '700',
        textAlign: 'center',
        marginTop: 20,
        borderRadius: 10,
    },
    searchbox:{
        fontSize: 20,
        fontWeight: '300',
        justifyContent: 'center',
        padding: 20,
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 8,
        marginBottom: 20,
    },
    results: {
        flex: 1,
    },
    result: {
        flex: 1,
        width: '100%',
        marginBottom: 20,
    },
    heading: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '700',
        padding: 20,
        backgroundColor:'#654317'
    }
})
