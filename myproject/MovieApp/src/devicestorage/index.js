import AsyncStorage from '@react-native-community/async-storage'

const devicesStorage = {
    async savetoken (key,value) {
        try{
            await AsyncStorage.setItem(key,value)
        }
        catch(error) {
            console.log('async',error)
        }
    },
    async gettoken (token){
        try {
            let value = await AsyncStorage.getItem(token)
            if (value !== null){
                return value
            }
        }
        catch(error) {
            console.log('token',error)
        }
    },
}
 
export default devicesStorage;